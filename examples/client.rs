use std::io;
use std::io::prelude::*;

extern crate dccp;

fn main() -> io::Result<()> {
    let mut client = dccp::DCCPSocket::connect("localhost:1234", 42)?;
    let mut buf = vec![0; 4096];
    let mut stdin = io::stdin();
    let mut stdout = io::stdout();

    loop {
        let mut n = stdin.read(&mut buf)?;
        client.write(&buf[..n])?;
        n = client.read(&mut buf)?;
        stdout.write(&buf[..n])?;
    }
}

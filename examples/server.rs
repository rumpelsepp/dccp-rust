use std::io;
use std::io::prelude::*;

extern crate dccp;

fn main() -> io::Result<()> {
    let server = dccp::DCCPListener::bind("localhost:1234", 42)?;
    let mut client = server.accept()?;
    let mut buf = vec![0; 4096];

    loop {
        let n = client.read(&mut buf)?;
        client.write(&buf[..n])?;
    }
}

